int redValue = 0x00;
int greenValue = 0x77;
int blueValue = 0xFF;

int redDirection = 1;
int greenDirection = 1;
int blueDirection = 1;

int redPin = 9;
int greenPin = 10;
int bluePin = 11;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}

void loop() {
  if (redValue == 0xFF)
    redDirection = -1;
  else if (redValue == 0)
    redDirection = 1;
  redValue = redValue + redDirection;
  analogWrite(redPin, redValue);

  if (greenValue == 0xFF)
    greenDirection = -1;
  else if (greenValue == 0)
    greenDirection = 1;
  greenValue = greenValue + greenDirection;
  analogWrite(greenPin, greenValue);

  if (blueValue == 0xFF)
    blueDirection = -1;
  else if (blueValue == 0)
    blueDirection = 1;
  blueValue = blueValue + blueDirection;
  analogWrite(bluePin, blueValue);

  delay(10); 
}

