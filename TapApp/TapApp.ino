/*
  TapApp (working title) Client based on Twitter Client with Strings
  
  Created 21 May 2011 by Tom Igoe
  Modified 22 Aug 2013 by Ray Saltrelli
  
  This code is in the public domain.
     
  This app waits for a button to be pressed to begin a timer. When button is released, if timer 
  exceeds threshold, the time is sent, along with a user and a tap, to the TapApp API to manage 
  stock and save flow history.
*/
  
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetClient.h>
#include <HttpClient.h>

#include <SPI.h>
#include <RFID.h>

// RFID 
#define SS_PIN 8
#define RST_PIN 9

int rfidNum0;

RFID rfid(SS_PIN, RST_PIN);

//  FLOW SIMULATION vars - jz
int USER_ID = 2; // 1 is jere, 2 is ray

int activeTap = 1;

int BUTTON_DOWN_THRESHOLD = 500; // in milliseconds - how long to consider it a pour
 
int tap1Pin = 2;  // the number of the input pin (for me a push button)
int tap2Pin = 7;

int current = LOW;
int previous = LOW;
int tap1Previous = LOW;
int tap2Previous = LOW;

int count;   // How long the button was held (secs)
  
unsigned long firstTime;   // how long since the button was first pressed 
// END FLOW SIMULATION vars

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x01 };
IPAddress ip(192,168,1,104);
IPAddress serverIP(174,120,0,189);
byte server[] = { 174,120,0,189 }; // My webpage IP

// initialize the library instance:
const char* serverName = "vircitystudios.com";  // my server - jz
 
void setup()
{  
  pinMode(tap1Pin,INPUT);  // 4-prong button
  pinMode(tap2Pin,INPUT);
  
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
 
  // attempt a DHCP connection:
  Serial.println("Attempting to get an IP address using DHCP:");
   
  if (!Ethernet.begin(mac))
  {
    // if DHCP fails, start with a hard-coded address:
    Serial.println("failed to get an IP address using DHCP, trying manually");
    Ethernet.begin(mac, ip);
  }
   
  Serial.print("My address:");
  Serial.println(Ethernet.localIP());
  
  SPI.begin();
  rfid.init();
}
 
void loop()
{
	
  checkForUserChange();

  determineTap();
  
  checkForUsage();
}
 
void checkForUserChange() {
	if(rfid.isCard()) {
          if(rfid.readCardSerial()){
             if(rfid.serNum[0] != rfidNum0){
			rfidNum0 = rfid.serNum[0];
			Serial.println(rfidNum0);
			// #todo -  replace with call to server user info based on id number
			if(rfidNum0 == 221){// blue token
				USER_ID = 1; 
			}else{ // white card
				USER_ID = 2;
			}				
		}
	  }
        }
} 
 
void determineTap() {
	current = digitalRead(tap1Pin);
	activeTap = 1;
	if(current == LOW && tap1Previous == LOW){
		current = digitalRead(tap2Pin);
		activeTap = 2;
	}
}

void checkForUsage(){

  if (current == HIGH && previous == LOW)
  {
    Serial.println("Button down");
    firstTime = millis();    // if the buttons becomes press remember the time 
  }
   
  if(current == HIGH)
  {
    count = millis() - firstTime; 
  }
   
  if (current == LOW && previous == HIGH)
  {
    Serial.println("Button up");
    Serial.println("Time: " + String(count) + " ms");

    if (count > BUTTON_DOWN_THRESHOLD)
    {
      connectToServer(count);
    }
    else
    {
      Serial.println("Threshold not met");
    }
    
    count = 0;
  } 
   previous = current;
   if(activeTap == 1){
		tap1Previous = previous;
	}else{
		tap2Previous = previous;
	}
} 
 
void connectToServer(int time)
{
  EthernetClient eClient;
  HttpClient httpClient(eClient);
  int error = 0;
  
  Serial.println("Initiate connection");
  Serial.println("Connecting");
  delay(1000); //This one keeps it from hanging
   
  char path[100];
  sprintf(path, "/swipeBrew/index.php/history/add/%d/%d/%d", USER_ID, time, activeTap);
  error = httpClient.get(serverName, 80, path);

  if (error < 0)
  {
    Serial.println("Request failed"); 
  }
  else
  {
    Serial.println("Request succeeded"); 
  }
  
  httpClient.stop();
}   
